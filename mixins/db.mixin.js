'use strict'

const DBService = require('moleculer-db')
// const DBService = require('../common/moleculer-auth-db')
const SqlAdapter = require('../common/sequelize-db-adaptor')
const DB = require('../db/operation/models')
const Promise = require('bluebird')
const {
  intersection,
  isArray,
  capitalize
} = require('lodash')
const {
  EntityNotFoundError
} = require('moleculer-db/src/errors')
const ApiGateway = require('moleculer-web')
const {
  UnAuthorizedError
} = ApiGateway.Errors
console.log(DB)
module.exports = function(model) {
  return {
    mixins: [DBService],
    actions: {
      create: {
        handler(ctx) {
          let entity = ctx.params
          return this.validateEntity(entity)
            .then(entity => this.adapter.insertPopulated(entity))
            .then(doc => this.transformDocuments(ctx, {}, doc))
            .then(json => this.entityChanged('created', json, ctx).then(() => json))
        }
      },
      update: {
        handler(ctx) {
          let id
          let sets = {}
          // Convert fields from params to '$set' update object
          Object.keys(ctx.params).forEach(prop => {
            if (prop === 'id' || prop === this.settings.idField) {
              id = this.decodeID(ctx.params[prop])
            } else {
              sets[prop] = ctx.params[prop]
            }
          })

          return this.getById(id, true)
            .then(doc => {
              if (ctx.meta.mineOrHasRole) {
                const minimumRoles = ctx.meta.mineOrHasRole
                const myId = ctx.meta.user.id
                const myRoles = ctx.meta.user.role

                if (!intersection(myRoles, minimumRoles).length) {
                  if (doc[this.settings.authorField || 'creator'] !== myId) {
                    return Promise.reject(new UnAuthorizedError())
                  }
                }
              }
            })
            .then(() => {
              return this.adapter.updateByIdPopulated(id, sets)
            })
            .then(doc => {
              if (!doc) {
                return Promise.reject(new EntityNotFoundError(id))
              }
              return this.transformDocuments(ctx, ctx.params, doc)
                .then(json => this.entityChanged('updated', json, ctx).then(() => json))
            })
        }
      },
      remove: {
        params: {
          id: {
            type: 'any'
          }
        },
        handler(ctx) {
          let params = this.sanitizeParams(ctx, ctx.params)
          const id = this.decodeID(params.id)
          return this.getById(id, true)
            .then(doc => {
              if (ctx.meta.mineOrHasRole) {
                const minimumRoles = ctx.meta.mineOrHasRole
                const myId = ctx.meta.user.id
                const myRoles = ctx.meta.user.role
                if (isArray(doc)) {
                  doc.map(eachDoc => {
                    if (!intersection(myRoles, minimumRoles).length) {
                      if (eachDoc[this.settings.authorField || 'creator'] !== myId) {
                        return Promise.reject(new UnAuthorizedError())
                      }
                    }
                  })
                } else {
                  if (!intersection(myRoles, minimumRoles).length) {
                    if (doc[this.settings.authorField || 'creator'] !== myId) {
                      return Promise.reject(new UnAuthorizedError())
                    }
                  }
                }
              }
            })
            .then(() => {
              if (isArray(id)) {
                return this.adapter.removeMany({
                  [this.settings.idField || 'id']: id
                })
              } else {
                return this.adapter.removeById(id)
              }
            })
            .then(doc => {
              if (!doc) {
                return Promise.reject(new EntityNotFoundError(params.id))
              }
              return this.transformDocuments(ctx, params, doc)
                .then(json => this.entityChanged('removed', json, ctx).then(() => json))
            })
        }
      }
    },
    methods: {
      entityChanged(type, json, ctx) {
        return this.clearCache().then(() => {
          const eventName = `entity${capitalize(type)}`
          // console.log(eventName, this.schema.methods)
          if (this.schema.methods[eventName] != null) {
            return this.schema.methods[eventName].call(this, json, ctx)
          }
        })
      }
    },
    adapter: new SqlAdapter(DB, model)
  }
}