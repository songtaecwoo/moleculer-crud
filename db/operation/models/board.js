'use strict';

module.exports = (sequelize, DataTypes) => {
  var board = sequelize.define('board', {
    user: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.STRING
  }, {});
  board.associate = function(models) {
    // associations can be defined here
  };
  return board;
};