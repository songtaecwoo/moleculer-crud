"use strict";

const ApiGateway = require("moleculer-web");

module.exports = {
  name: "api",
  mixins: [ApiGateway],

  // More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
  settings: {
    port: process.env.PORT || 3000,

    routes: [{
      path: '/',
      aliases: {
        'GET board': 'boards.list',
        'POST board': 'boards.create',
        'GET board/:id': 'boards.get',
        'PUT board/:id': 'boards.update',
        'DELETE board/:id': 'boards.remove'
      },
      mappingPolicy: 'restrict',
      cors: true,
      bodyParsers: {
        json: true,
        urlencoded: {
          extended: true
        }
      }
    }],

    // Serve assets from "public" folder
    assets: {
      folder: "public"
    }
  }
};