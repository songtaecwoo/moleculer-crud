'use strict'

const DbService = require('../mixins/db.mixin')

module.exports = {
  name: 'boards',
  mixins: [DbService('board')]
}